﻿namespace GraphQLTest.Domain
{
    public class Book
    {
        public int Id { get; set; }

        public string Title { get; set; }  

        public int GenreId { get; set; }

        public Genre Genre { get; set; }

        public string Description { get; set; } 

    }
}