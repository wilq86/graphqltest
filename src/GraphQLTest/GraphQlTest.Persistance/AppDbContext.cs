﻿using GraphQLTest.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphQlTest.Persistance
{
    public class AppDbContext : DbContext
    {
        public DbSet<Book> Books { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options)
                : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Book>().HasData(
                new Genre()
                {
                    Id = 1,
                    Description = "Description 1",
                    Name = "Name 1",
                },

                new Genre()
                {
                    Id = 2,
                    Description = "Description 2",
                    Name = "Name 2",
                }
                );

            modelBuilder.Entity<Book>().HasData(
                new Book()
                {
                    Id = 1,
                    Description = "Description 1",
                    Title = "Title 1",
                },

                new Book()
                {
                    Id = 2,
                    Description = "Description 2",
                    Title = "Title 2",
                }
                );

            base.OnModelCreating(modelBuilder);
        }
    }
}
