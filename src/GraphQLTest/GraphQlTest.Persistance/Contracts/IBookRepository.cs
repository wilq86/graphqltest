﻿using GraphQLTest.Domain;

namespace GraphQlTest.Persistance.Contracts
{
    public interface IBookRepository
    {
        Task<List<Book>> GetAll();
    }
}