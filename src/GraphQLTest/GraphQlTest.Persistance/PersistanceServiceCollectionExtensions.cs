﻿using GraphQlTest.Persistance.Contracts;
using GraphQlTest.Persistance.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphQlTest.Persistance
{
    public static class PersistanceServiceCollectionExtensions
    {
        public static IServiceCollection AddPersistance(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<AppDbContext>(options =>
            options.UseSqlite(configuration["ConnectionStrings:DB"]));

            services.AddScoped<IBookRepository, BookRepository>();

            return services;
        }
    }
}
