﻿using GraphQL.Types;
using GraphQlTest.Persistance;
using GraphQlTest.Persistance.Repositories;

namespace GraphQLTest.Api.GraphQl
{
    public class BookSchema : Schema
    {
        private readonly AppDbContext context;

        public BookSchema(AppDbContext dbContext) : base()
        {
            context = dbContext;

            Query = new BookQuery
               (new BookRepository(context));
        }
    }
}
