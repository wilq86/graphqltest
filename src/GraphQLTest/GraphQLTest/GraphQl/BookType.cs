﻿using GraphQL.Types;
using GraphQLTest.Domain;

namespace GraphQLTest.Api.GraphQl
{
    public class BookType : ObjectGraphType<Book>
    {
        public BookType()
        {
            Field(t => t.Id);
            Field(t => t.Title).Description("The name of the draon");
            Field(t => t.Description);
            //Field(t => t.IntroducedAt).Description("When the dragon was first introduced in the catalog");
            //Field(t => t.Price);
            //Field(t => t.Rating).Description("The (max 5) star customer rating");
            //Field<ColorDragonType>("Color", "The color of dragon");
            //Field<BreathDragonType>("Breath", "The breath of dragon");
        }
    }
}
