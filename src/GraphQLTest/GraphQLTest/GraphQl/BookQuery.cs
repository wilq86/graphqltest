﻿using GraphQL.Types;
using GraphQlTest.Persistance.Repositories;
using GraphQLTest.Domain;

namespace GraphQLTest.Api.GraphQl
{
    public class BookQuery : ObjectGraphType
    {
        public BookQuery(BookRepository bookRepository)
        {
            Field<ListGraphType<BookType>>(
                "books",
                resolve: context => bookRepository.GetAll()
            );
        }
    }
}
