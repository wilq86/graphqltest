using GraphQL;
using GraphQlTest.Persistance;
using GraphQLTest.Api.GraphQl;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddPersistance(builder.Configuration);
builder.Services.AddScoped<BookSchema>();

builder.Services.AddGraphQL(builder =>
{
    builder.AddSchema<BookSchema>();
    builder.AddGraphTypes(typeof(BookQuery).Assembly);
    // .AddGraphTypes(ServiceLifetime.Scoped);
    //builder.AddNewtonsoftJson();
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
